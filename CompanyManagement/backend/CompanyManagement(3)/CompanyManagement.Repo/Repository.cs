﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyManagement.Repo
{
    public class Repository : IRepository
    {
        private readonly IGraphRepository _graphRepository;

        public Repository(IGraphRepository graphRepository)
        {
            _graphRepository = graphRepository;
        }
    }
}
