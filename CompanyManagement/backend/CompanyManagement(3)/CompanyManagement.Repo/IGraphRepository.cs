﻿using System;
using System.Collections.Generic;
using System.Text;
using Neo4j.Driver.V1;
using Neo4jClient;

namespace CompanyManagement.Repo
{
    public interface IGraphRepository
    {
        IGraphClient GraphClient { get; set; }
        IDriver Driver { get; set; }
    }
}
