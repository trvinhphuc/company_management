﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CompanyManagement.Repo;
using CompanyManagement.Data;
using System.Threading.Tasks;

namespace CompanyManagement.Service
{
    public class CompanyService : ICompanyService
    {
        private IGraphRepository _GraphRepository;
        public CompanyService(IGraphRepository graphRepository) 
        {
            this._GraphRepository = graphRepository;
        }
        public async Task<IEnumerable<Company>> GetAllCompanyAsync()
        {
            var resultClient = await _GraphRepository.GraphClient.Cypher
                .Match($"((c:{Company.LABEL_COMPANY}))")
                .ReturnDistinct(c => c.As<Company>())
                .ResultsAsync;
            return resultClient.ToList();
        }

        public async Task CreateCompanyAsync(Company companyObject)
        {
            var query = _GraphRepository.GraphClient.Cypher
                    .Create($"((c:{Company.LABEL_COMPANY}{{newCompany}}))")
                    .WithParam("newCompany", companyObject)
                    .ReturnDistinct(c => c.As<Company>());

            var result = await query.ResultsAsync;

        }

        public async Task DeleteCompanyAsync(Guid globalId)
        { 
            var query = _GraphRepository.GraphClient.Cypher
            .Match($"(c:{Company.LABEL_COMPANY})")
            .Where((Company c) => c.id == globalId)
            .DetachDelete("c");

            await query.ExecuteWithoutResultsAsync();
        }

        public async Task<bool> UpdateCompany(Guid id , Company companyObject)
        {
            var query = _GraphRepository.GraphClient.Cypher
                    .Match($"((c:{Company.LABEL_COMPANY}))")
                    .Where((Company c) => c.id == id)
                    .Set("c = {company}")
                    .WithParam("company", new Company
                    {
                        id = id,
                        name = companyObject.name,
                        description = companyObject.description,
                        dateTimeCreated = companyObject.dateTimeCreated
                    });
            var resultClient = query.ExecuteWithoutResultsAsync();
            if (resultClient.IsCompletedSuccessfully)
            {
                return true;
            }
            return false;
        }
    }
}
