﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CompanyManagement.Data;
namespace CompanyManagement.Service
{
    public interface ICompanyService
    {
        Task<IEnumerable<Company>> GetAllCompanyAsync();
        Task CreateCompanyAsync(Company newCom);
        Task DeleteCompanyAsync(Guid globalId);
        Task<bool> UpdateCompany(Guid id, Company companyObject);
    }
}
