﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyManagement.Models
{
    public class CompanyModel
    {
        public const string LABEL_COMPANY = "Company";
        public Guid id { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public string dateTimeCreated { get; set; }
    }
}
