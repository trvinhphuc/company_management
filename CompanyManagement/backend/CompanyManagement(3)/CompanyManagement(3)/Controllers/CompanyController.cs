﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Neo4j.Driver.V1;
using CompanyManagement.Models;
using Neo4jClient;
using Newtonsoft.Json;
using CompanyManagement.Data;
using CompanyManagement.Service;

namespace CompanyManagement.Controllers
{
   
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private IDriver _driver;
        private readonly IGraphClient _graphClient;

        private readonly ICompanyService _company;

        public CompanyController(ICompanyService company)
        {
            this._company = company;
        }
        // GET api/<controller>
        public async Task<IActionResult> Get()
        {
            try
            {
                var listCompany = await _company.GetAllCompanyAsync();
                return Ok(listCompany);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        // POST api/<controller>
        [HttpPost]
        public Object Post([FromBody]CompanyModel companyObject)
        {
            await _company.CreateCompanyAsync(companyObject);
            return Ok();
        }
        // PUT api/<controller>/id
        [HttpPut("{id}")]
        public Object Put(Guid id, [FromBody] CompanyModel companyObject)
        {
            bool flag = await _company.UpdateCompany(id,companyObject);
            if (flag) return Ok("successful");
            return BadRequest("Can't update company");
        }
        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public Object Delete(Guid id)
        {
            try
            {
                await _company.DeleteCompanyAsync(id);
                return Ok("remove object successful");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}