﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyManagement.Data
{
    public class Company
    {
        public const string LABEL_COMPANY = "Company";
        public Guid id { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public string dateTimeCreated { get; set; }
    }
}
