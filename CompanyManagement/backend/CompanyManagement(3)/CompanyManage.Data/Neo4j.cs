﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyManagement.Data
{
    public class Neo4j
    {
        public string Uri { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
