import { Actions } from '@ngrx/effects';
import { BaseSuccessAction, BaseFailedAction } from './action.interface';

export class BaseSelector {
  constructor(
    protected actions: Actions,
    protected actionSuccessType: string,
    protected actionFailedType: string
  ) { }

  actionSuccessOfSubtype$ = (...typeNames: string[]) =>
    this.actions.ofType(this.actionSuccessType)
      .map(action => action as BaseSuccessAction)
      .filter(a => typeNames.includes(a.subType));

  actionFailedOfSubtype$ = (...typeNames: string[]) =>
    this.actions.ofType(this.actionFailedType)
      .map(action => action as BaseFailedAction)
      .filter(a => typeNames.includes(a.subType));

  actionOfType$ = (...typeNames: string[]) =>
    this.actions.filter(a => typeNames.includes(a.type));
}