import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import 'rxjs/Rx';
import { Company } from "../models/company.model";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  private companyUrl = "https://localhost:44308/api/company/";
  private companyDeleteUrl = "https://localhost:44308/api/deleteCompany";
  constructor(
    private http : HttpClient
  ) { }

  GetCompanyList(){
    return this.http.get(this.companyUrl + "get");
    // .catchError(this.handleError);
  }

  createNewCompany( companyInfo : any ){
    return this.http.post(this.companyUrl + "post",companyInfo);
  }

  deleteCompany( companyInfo ){
    return this.http.delete(this.companyUrl + "delete" + "/" + companyInfo.id);
  }

  updateCompany( companyInfo ){
    return this.http.put(this.companyUrl + "put" + "/" + companyInfo.id,companyInfo);
  }

  protected handleError(error: HttpErrorResponse) {
    return Observable.throw(error);
  }
}
