import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule  } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { Dispatcher } from "./main-page/main-page.dispatcher";
import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from "@angular/common/http";
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppEffects } from "./main-page/main-page.state/app.effects";
import { appModule } from "./main-page/main-page.state/app.reducer";
import { AppSelectors } from "./main-page/main-page.state/app.selectors";

import { OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { NotifierModule } from 'angular-notifier';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent
  ],
  imports: [
    BrowserModule,
    NgxDatatableModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forRoot([AppEffects]),
    StoreModule.forRoot({
      app: appModule
    }),
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    OwlMomentDateTimeModule,
    NotifierModule
  ],
  providers: [
    Dispatcher,
    AppSelectors
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
