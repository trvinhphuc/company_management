import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { CompanyService } from '../service/company.service';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Company } from "../models/company.model";
import { FormGroup, FormControl } from '@angular/forms';
import { Dispatcher } from "./main-page.dispatcher";
import { Store } from "@ngrx/store";
import {
  AppActionNames,
  GetCompanyListAction,
  CreateNewCompanyAction,
  UpdateCompanyAction,
  DeleteCompanyAction
} from "./main-page.state/app.actions";
import { AppSelectors } from "./main-page.state/app.selectors";
import { Guid } from "guid-typescript";
import { Subscription } from 'rxjs/Subscription';
import { NotifierService } from 'angular-notifier';
import * as resizer from 'iframe-resizer';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  providers: [CompanyService]
})
export class MainPageComponent implements OnInit {

  constructor(
    private companyService: CompanyService,
    private modalService: BsModalService,
    private dispatcher: Dispatcher,
    private store: Store<any>,
    private selector: AppSelectors,
    private notifier: NotifierService
  ) {
    this.getCompanyList();
  }
  @ViewChild('myIframe') a: ElementRef;
  iframeResizer: any;
  ngOnInit() {
    this.dispatcher.fire(new GetCompanyListAction());
    resizer.iframeResizer({
      log: true,
      autoResize: true
    }, this.a.nativeElement);

  }
  test(event) {
    console.log(event);
  }
  companyList: any;
  private getCompanyList(): Subscription[] {
    return [
      this.selector.actionSuccessOfSubtype$(AppActionNames.GET_COMPANY_LIST)
        .subscribe(data => {
          this.companyList = data.payload;
          this.companyList.forEach(row => {
            row.dateTimeCreated = new Date(row.dateTimeCreated * 1000).toLocaleString();
          });
        }),
      this.selector.actionSuccessOfSubtype$(AppActionNames.UPDATE_COMPANY)
        .subscribe(() => {
          this.dispatcher.fire(new GetCompanyListAction());
        }),
      this.selector.actionSuccessOfSubtype$(AppActionNames.CREATE_NEW_COMPANY)
        .subscribe(() => {
          this.dispatcher.fire(new GetCompanyListAction());

        })
    ]
  }
  editCompany() {
    if (this.companyDetail.value.name == null
      || this.companyDetail.value.name == ''
      || this.companyDetail.value.description == null
      || this.companyDetail.value.description == '') {
      alert('Company name and description should not be empty');
    }
    else {
      if (this.currentCompany.id) {
        this.dispatcher.fire(new UpdateCompanyAction(
          {
            id: this.currentCompany.id,
            name: this.companyDetail.value.name,
            description: this.companyDetail.value.description,
            dateTimeCreated: new Date(this.currentCompany.dateTimeCreated).getTime() / 1000
          }
        ))

      }
      else {
        this.dispatcher.fire(new CreateNewCompanyAction(
          {
            id: Guid.create().toString(),
            name: this.companyDetail.value.name,
            description: this.companyDetail.value.description,
            dateTimeCreated: new Date().getTime() / 1000
          }
        ));
      }
      this.notifier.notify('success', 'Success!');
      this.modalRef.hide()
    }

  }
  deleteCompany() {
    this.dispatcher.fire(new DeleteCompanyAction(this.currentCompany))
    this.selector.actionSuccessOfSubtype$(AppActionNames.DELETE_COMPANY)
      .subscribe(() => {
        this.dispatcher.fire(new GetCompanyListAction());
        this.modalRef.hide();
      })
    this.notifier.notify('success', 'Success!');
  }
  currentCompany: Company;
  companyDetail: FormGroup;
  modalRef: BsModalRef;

  openModalWithClassAndData(template: TemplateRef<any>, companyData: Company) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' })
    );
    if (companyData) {
      this.currentCompany = companyData;
      this.companyDetail = new FormGroup({
        'name': new FormControl(companyData.name),
        'description': new FormControl(companyData.description),
      })
    }
    else {
      var emptyCompany = new Company(null, null, null, null);
      this.currentCompany = emptyCompany;
      this.companyDetail = new FormGroup({
        'name': new FormControl(this.currentCompany.name),
        'description': new FormControl(this.currentCompany.description),
      })
    }
  }
}
