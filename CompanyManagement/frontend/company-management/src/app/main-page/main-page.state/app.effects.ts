import { Actions, Effect, ofType } from '@ngrx/effects';
import { withLatestFrom, switchMap, map, mergeMap, concatMap, filter, tap } from 'rxjs/operators';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AppActionNames,
  AppSuccessAction,
  GetCompanyListAction,
  CreateNewCompanyAction,
  UpdateCompanyAction,
  DeleteCompanyAction
} from './app.actions';
import { Dispatcher } from "../main-page.dispatcher";
import { CompanyService } from "../../service/company.service";

@Injectable()
export class AppEffects {

  constructor(
    private actions$: Actions,
    private dispatcher: Dispatcher,
    private companyService: CompanyService
  ) { }

  @Effect()
  companyList$ = this.actions$.pipe(
    ofType(AppActionNames.GET_COMPANY_LIST),
    switchMap((action: GetCompanyListAction) => {
      return this.companyService
        .GetCompanyList()
        .map(companyList => {
          return new AppSuccessAction(AppActionNames.GET_COMPANY_LIST, companyList);
        })
    })
  )
  
  @Effect()
  createNewCompany$ = this.actions$.pipe(
    ofType(AppActionNames.CREATE_NEW_COMPANY),
    switchMap((action: CreateNewCompanyAction) => {
      return this.companyService.createNewCompany(action.payload)
      .map(result => new AppSuccessAction(AppActionNames.CREATE_NEW_COMPANY,result))
    })
  )

  @Effect()
  updateCompany$ = this.actions$.pipe(
    ofType(AppActionNames.UPDATE_COMPANY),
    switchMap((action: UpdateCompanyAction) => {
      return this.companyService.updateCompany(action.payload)
      .map(result => new AppSuccessAction(AppActionNames.UPDATE_COMPANY,result))
    })
  )

  @Effect()
  deleteCompany$ = this.actions$.pipe(
    ofType(AppActionNames.DELETE_COMPANY),
    switchMap((action: DeleteCompanyAction) => {
      return this.companyService.deleteCompany(action.payload)
      .map(result => new AppSuccessAction(AppActionNames.DELETE_COMPANY,result))
    })
  )
}
