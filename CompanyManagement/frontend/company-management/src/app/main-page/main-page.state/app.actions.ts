import { Action } from '@ngrx/store';

export enum AppActionNames {
  GET_COMPANY_LIST = '[APP] Get company list',
  CREATE_NEW_COMPANY = '[APP] Create new company',
  UPDATE_COMPANY = '[APP] Update company',
  DELETE_COMPANY = '[APP] Delete company',
  ACTION_SUCCESS = '[APP] Action Success',
  ACTION_FAILED = '[APP] Action Failed'
}

export interface AppAction extends Action {
  type: AppActionNames;
  payload?: any;
}

export class GetCompanyListAction implements Action{
  type = AppActionNames.GET_COMPANY_LIST;
  constructor() {}
}
export class CreateNewCompanyAction implements Action{
  type = AppActionNames.CREATE_NEW_COMPANY;
  constructor(public payload: any) {}
}
export class UpdateCompanyAction implements Action{
  type = AppActionNames.UPDATE_COMPANY;
  constructor(public payload: any) {}
}

export class DeleteCompanyAction implements Action{
  type = AppActionNames.DELETE_COMPANY;
  constructor(public payload: any) {}
}

export class AppSuccessAction implements Action{
  type = AppActionNames.ACTION_SUCCESS;
  constructor(public subType: AppActionNames, public payload: any) { }
}
export class AppFailedAction implements Action{
  type = AppActionNames.ACTION_FAILED;
  constructor(public subType: AppActionNames, public payload: any) { }
}


