import { Company } from "../../models/company.model";
// the interface for IAppState
export interface IAppState {
  companyList: Company[];
}
