import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Actions } from '@ngrx/effects';
import { AppActionNames } from './app.actions';
import { Company } from "../../models/company.model";
import { BaseSelector } from "../../service/base.selector";

@Injectable()
export class AppSelectors extends BaseSelector{

  public companyList$ : Observable<Company[]>;

  constructor(
    private store: Store<any>,
    private appActions: Actions,
  ) {
    super(appActions, AppActionNames.ACTION_SUCCESS, AppActionNames.ACTION_FAILED);
    this.companyList$ = this.store.select(x => x.app.companyList);
    
  }
}
