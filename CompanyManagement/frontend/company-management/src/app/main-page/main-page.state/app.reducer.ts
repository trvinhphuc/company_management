import { IAppState } from './app.state';
import {
  AppAction,
  AppActionNames,
  AppSuccessAction,
  AppFailedAction
} from './app.actions';
import { Company } from "../../models/company.model";
// initial state
export const initialAppState: IAppState = {
  companyList: []
};

// reducer
export function appModule(appState: IAppState = initialAppState, action: AppAction): IAppState {
  switch (action.type) {
    case AppActionNames.ACTION_SUCCESS:
      return actionSuccessReducer(appState, action as AppSuccessAction);
    default:
      return appState;
  }
}

function actionSuccessReducer(appState: IAppState, action: AppSuccessAction): IAppState {
  switch (action.subType) {
    case AppActionNames.GET_COMPANY_LIST:
      return {
        ...appState,
        companyList: action.payload,
      };
    case AppActionNames.CREATE_NEW_COMPANY:
      return {
        ...appState,
      };
    case AppActionNames.UPDATE_COMPANY:
      return {
        ...appState
      };
    case AppActionNames.DELETE_COMPANY:
      return {
        ...appState
      };
    default:
      return appState;
  }
}
function actionFailReducer(appState: IAppState, action: AppFailedAction): IAppState {
  switch (action.subType) {
    default:
      return appState;
  }
}


