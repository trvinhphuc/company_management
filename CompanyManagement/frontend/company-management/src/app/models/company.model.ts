import { Guid } from "guid-typescript";

export class Company {
    id : Guid;
    name : string;
    description : string;
    dateTimeCreated : Date;

    constructor(
        id: Guid,
        name : string,
        description : string,
        dateTimeCreated : Date ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateTimeCreated = dateTimeCreated;
    }
}